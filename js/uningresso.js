$("#btn-cupom").on('click', function(){
    $(".alert-cupom").children().remove()

    var html =  '<form class="fomr-cupom" method="post" action="/cupom/apply">' +
                    '<div class="row">'+
                        '<div class="col-md-8">'+
                            ' <div class="form-group" style="margin-bottom: initial;">'+
                                '<label>Digite seu Cupom</label>'+
                                '<input placeholder="00000-0" class="form-control" id="cupom" name="cupom" type="text" style="text-align: center" maxlength="7">'+
                            '</div>'+
                        '</div>'+
                        '<div class="col-md-4"><br>' +
                            '<button type="submit" class="btn  btn-danger btn-outline" style="margin-top: 3px">'+
                                '<span>Aplicar</span>'+
                            '</button>'+
                        '</div>'+
                    '</div>'+
                '</form>';

    $(".alert-cupom").append(html);
});


$(document).ready(function() {
    $('#CPF').mask('000.000.000-00', {reverse: true});
    $('#Nascimento').mask('00/00/0000');
    $('#Telefone').mask("(00)00000-0000");
    $('#Celular').mask("(00)00000-0000");
    $('#cupom').mask("00000-0");

});

function getCities(param){
    
    
    var html = '  <div class="col-md-6 col-sm-6 col-xs-12">\
                    <div class="product product-grid" style="background-color: #00000080">\
                    <div class="product-body" style="padding: 5px;">\
                    <h2 class="product-name">\
                    <a href="javascript:void(0)" title="NIGHT BASS - 2 ANOS !!!">Shopping Total <br>&nbsp;</a>\
                    </h2>\
                    </div>\
                    <div class="product-media">\
                        <div class="product-thumbnail">\
                        <a href="#!" title="NIGHT BASS - 2 ANOS !!!">\
                        <img src="http://www.uningressos.com.br/principal/pub/Image/20160501130518dados_empresa_1379621718_655.png" alt="NIGHT BASS - 2 ANOS !!!" class="current" style="max-height: 120px;min-height: 120px">\
                        </a>\
                        </div>\
                        </div>\
                        <div class="product-body" style="padding: 10px;">\
                        <div class="product-category" style="font-size: 12px">\
                        <span>Curitiba - PR</span>\
                        </div>\
                        <div class="product-price" style="color:#fff">\
                        <span class="amount">Loja Uningressos</span>\
                    </div>\
                    <div class="product-price" style="color:#E67817; font-size: 12px;">\
                        <span class="amount">R. Itacolomi, 292, Loja 306</span><br>\
                    <span class="amount">Fone: (41) 3086-0063</span><br>\
                    </div>\
                    </div>\
                    </div>\
                    </div>'
    
}

$(".cep").on('change', function(){
    $.get( "https://cep-bemean.herokuapp.com/api/br/81050685", function(data){
        $("#endereco").val(data.address);
        $("#cep").val(data.code);
        $("#bairro").val(data.district);
        $("#estado").val(data.state);
        $("#cidade").val(data.city);
    });
});

$("#cep").on('change', function(){
    $.get( "https://cep-bemean.herokuapp.com/api/br/81050685", function(data){
        $("#endereco").val(data.address);
        $("#cep").val(data.code);
        $("#bairro").val(data.district);
        $("#estado").val(data.state);
        $("#cidade").val(data.city);
    });
});

$('.add-address').on ('click', function(){

    $("#to-add-address").empty();

    var html = "<form action=\"/checkout\" method=\"POST\" class=\"form-address\">\
                <div class=\"row\">\
        <div class=\"col-md-8\">\
        <div class=\"form-group\" style=\"margin-bottom: initial;\">\
        <label for=\"endereco\">Rua</label><br>\
        <input placeholder=\"Rua\" class=\"form-control\" id=\"endereco\" name=\"endereco\" type=\"text\" required>\
        </div>\
        </div>\
        <div class=\"col-md-4\">\
        <div class=\"form-group\" style=\"margin-bottom: initial;\">\
        <label for=\"numero\">N°</label><br>\
        <input placeholder=\"Digite N°\" class=\"form-control\" id=\"numero\" name=\"numero\" type=\"text\" required>\
        </div>\
        </div>\
        </div>\
        <br>\
        <div class=\"row\">\
        <div class=\"col-md-4\">\
        <div class=\"form-group\" style=\"margin-bottom: initial;\">\
        <label for=\"cep\" name=\"cep\">CEP</label><br>\
        <input placeholder=\"Digite seu CEP\" class=\"form-control cep\" id=\"cep\" name=\"cep\" type=\"text\" required>\
        </div>\
        </div>\
        <div class=\"col-md-4\">\
        <div class=\"form-group\" style=\"margin-bottom: initial;\">\
        <label for=\"bairro\" name=\"bairro\">Bairro</label><br>\
        <input placeholder=\"Digite Bairro\" class=\"form-control\" id=\"bairro\" name=\"bairro\" type=\"text\" required>\
        </div>\
        </div>\
        <div class=\"col-md-4\">\
        <div class=\"form-group\" style=\"margin-bottom: initial;\">\
        <label for=\"complemento\" name=\"complemento\">Complemento</label><br>\
        <input placeholder=\"Digite Complemento\" class=\"form-control\" id=\"complemento\" name=\"complemento\" type=\"text\">\
        </div>\
        </div>\
        </div>\
        <br>\
        <div class=\"row\">\
        <div class=\"col-md-4\">\
        <div class=\"form-group\" style=\"margin-bottom: initial;\">\
        <label for=\"estado\">Estado</label><br>\
        <select name=\"estado\" id=\"estado\" class=\"form-control\">\
        <option value=\"\">Selecione</option>\
        <option value=\"AC\">AC</option>\
        <option value=\"AL\">AL</option>\
        <option value=\"AM\">AM</option>\
        <option value=\"AP\">AP</option>\
        <option value=\"BA\">BA</option>\
        <option value=\"CE\">CE</option>\
        <option value=\"DF\">DF</option>\
        <option value=\"ES\">ES</option>\
        <option value=\"GO\">GO</option>\
        <option value=\"MA\">MA</option>\
        <option value=\"MG\">MG</option>\
        <option value=\"MS\">MS</option>\
        <option value=\"MT\">MT</option>\
        <option value=\"PA\">PA</option>\
        <option value=\"PB\">PB</option>\
        <option value=\"PE\">PE</option>\
        <option value=\"PI\">PI</option>\
        <option value=\"PR\">PR</option>\
        <option value=\"RJ\">RJ</option>\
        <option value=\"RN\">RN</option>\
        <option value=\"RS\">RS</option>\
        <option value=\"RO\">RO</option>\
        <option value=\"RR\">RR</option>\
        <option value=\"SC\">SC</option>\
        <option value=\"SE\">SE</option>\
        <option value=\"SP\">SP</option>\
        <option value=\"TO\">TO</option>\
        </select>\
        </div>\
        </div>\
        <div class=\"col-md-8\">\
        <div class=\"form-group\" style=\"margin-bottom: initial;\">\
        <label for=\"cidade\">Cidade</label><br>\
        <input placeholder=\"Digite sua Cidade\" class=\"form-control\" id=\"cidade\" name=\"cidade\" type=\"text\" required>\
        </div>\
        </div>\
        </div>\
        <br>\
        </form>";

    $("#to-add-address").append(html);
});
